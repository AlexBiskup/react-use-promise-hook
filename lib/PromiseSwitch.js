"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UsePromise_1 = require("./UsePromise");
var React = require("react");
function PromiseSwitch(props) {
    var promise = props.promise, createOnNotCreated = props.createOnNotCreated;
    var state = promise[0], createPromise = promise[1];
    var defaults = React.useContext(PromiseSwitchContext);
    function renderNotCreated() {
        if (props.renderOnNotCreated) {
            return props.renderOnNotCreated(createPromise);
        }
        if (defaults.renderOnNotCreated) {
            return defaults.renderOnNotCreated(createPromise);
        }
        return null;
    }
    function renderPending() {
        if (props.renderOnPending) {
            return props.renderOnPending();
        }
        if (defaults.renderOnPending) {
            return defaults.renderOnPending();
        }
        return null;
    }
    function renderFulfilled(value) {
        if (props.children) {
            return props.children(value, createPromise);
        }
        if (props.renderOnFulfilled) {
            return props.renderOnFulfilled(value, createPromise);
        }
        if (defaults.renderOnFulfilled) {
            return defaults.renderOnFulfilled(value, createPromise);
        }
        return null;
    }
    function renderRejected(reason) {
        if (props.renderOnRejected) {
            return props.renderOnRejected(reason, createPromise);
        }
        if (defaults.renderOnRejected) {
            return defaults.renderOnRejected(reason, createPromise);
        }
        return null;
    }
    switch (state.status) {
        case UsePromise_1.PromiseStatus.NotCreated: {
            if (createOnNotCreated) {
                createOnNotCreated(createPromise);
            }
            return renderNotCreated();
        }
        case UsePromise_1.PromiseStatus.Pending:
            {
                return renderPending();
            }
            ;
        case UsePromise_1.PromiseStatus.Fulfilled: {
            return renderFulfilled(state.value);
        }
        case UsePromise_1.PromiseStatus.Rejected: {
            return renderRejected(state.reason);
        }
    }
}
exports.PromiseSwitch = PromiseSwitch;
var PromiseSwitchContext = React.createContext({});
function PromiseSwitchDefaultsProvider(props) {
    var defaults = props.defaults, children = props.children;
    return (React.createElement(PromiseSwitchContext.Provider, { value: defaults }, children));
}
exports.PromiseSwitchDefaultsProvider = PromiseSwitchDefaultsProvider;
