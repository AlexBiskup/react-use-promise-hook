/// <reference types="react" />
import { CreatePromiseFunction, PromiseState } from './UsePromise';
declare type RenderResult = JSX.Element | null;
declare type Props<T, A extends any[]> = {
    promise: [PromiseState<T>, CreatePromiseFunction<T, A>];
    createOnNotCreated?: (createPromise: CreatePromiseFunction<T, A>) => Promise<T>;
    renderOnNotCreated?: (createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    renderOnPending?: () => RenderResult;
    renderOnRejected?: (reason: any, createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    renderOnFulfilled?: (value: T, createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    children?: (value: T, createPromise: CreatePromiseFunction<T, A>) => RenderResult;
};
export declare function PromiseSwitch<T, A extends any[]>(props: Props<T, A>): RenderResult;
export declare type PromiseSwitchDefaults = {
    renderOnNotCreated?: (createPromise: CreatePromiseFunction<any, any>) => RenderResult;
    renderOnPending?: () => RenderResult;
    renderOnRejected?: (reason: any, createPromise: CreatePromiseFunction<any, any>) => RenderResult;
    renderOnFulfilled?: (value: any, createPromise: CreatePromiseFunction<any, any>) => RenderResult;
};
declare type ProviderProps = {
    defaults: PromiseSwitchDefaults;
    children: JSX.Element;
};
export declare function PromiseSwitchDefaultsProvider(props: ProviderProps): JSX.Element;
export {};
