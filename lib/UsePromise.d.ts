export declare type PromiseState<T> = PromiseNotCreatedState | PromisePendingState | PromiseFulfilledState<T> | PromiseRejectedState;
export declare type PromiseFulfilledState<T> = {
    status: PromiseStatus.Fulfilled;
    value: T;
};
export declare type PromiseRejectedState = {
    status: PromiseStatus.Rejected;
    reason: any;
};
export declare type PromiseNotCreatedState = {
    status: PromiseStatus.NotCreated;
};
export declare type PromisePendingState = {
    status: PromiseStatus.Pending;
};
export declare type CreatePromiseFunction<T, A extends any[]> = (...args: A) => Promise<T>;
export declare enum PromiseStatus {
    NotCreated = "NOT_CREATED",
    Pending = "PENDING",
    Fulfilled = "FULFILLED",
    Rejected = "REJECTED"
}
export declare function usePromise<T, A extends any[]>(createPromise: CreatePromiseFunction<T, A>): [PromiseState<T>, CreatePromiseFunction<T, A>];
