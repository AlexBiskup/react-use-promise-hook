"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var PromiseStatus;
(function (PromiseStatus) {
    PromiseStatus["NotCreated"] = "NOT_CREATED";
    PromiseStatus["Pending"] = "PENDING";
    PromiseStatus["Fulfilled"] = "FULFILLED";
    PromiseStatus["Rejected"] = "REJECTED";
})(PromiseStatus = exports.PromiseStatus || (exports.PromiseStatus = {}));
function usePromise(createPromise) {
    function createPromiseWrapper() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return new Promise(function (resolve, reject) {
            setState({
                status: PromiseStatus.Pending
            });
            createPromise.apply(void 0, args).then(function (value) {
                setState({
                    status: PromiseStatus.Fulfilled,
                    value: value,
                });
                resolve(value);
            }).catch(function (reason) {
                setState({
                    status: PromiseStatus.Rejected,
                    reason: reason,
                });
                reject(reason);
            });
        });
    }
    var defaultState = {
        status: PromiseStatus.NotCreated,
    };
    var _a = react_1.useState(defaultState), state = _a[0], setState = _a[1];
    return [state, createPromiseWrapper];
}
exports.usePromise = usePromise;
