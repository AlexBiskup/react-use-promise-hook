import { CreatePromiseFunction, PromiseStatus, PromiseState } from './UsePromise';
import * as React from 'react';

type RenderResult = JSX.Element | null;

type Props<T, A extends any[]> = {
    promise: [PromiseState<T>, CreatePromiseFunction<T, A>];
    createOnNotCreated?: (createPromise: CreatePromiseFunction<T, A>) => Promise<T>;
    renderOnNotCreated?: (createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    renderOnPending?: () => RenderResult;
    renderOnRejected?: (reason: any, createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    renderOnFulfilled?: (value: T, createPromise: CreatePromiseFunction<T, A>) => RenderResult;
    children?: (value: T, createPromise: CreatePromiseFunction<T, A>) => RenderResult;

}

export function PromiseSwitch<T, A extends any[]>(props: Props<T, A>) {

    const { promise, createOnNotCreated } = props;
    const [state, createPromise] = promise;

    const defaults = React.useContext(PromiseSwitchContext);

    function renderNotCreated(): RenderResult {
        if (props.renderOnNotCreated) {
            return props.renderOnNotCreated(createPromise);
        }
        if (defaults.renderOnNotCreated) {
            return defaults.renderOnNotCreated(createPromise)
        }
        return null;
    }

    function renderPending(): RenderResult {
        if (props.renderOnPending) {
            return props.renderOnPending();
        }
        if (defaults.renderOnPending) {
            return defaults.renderOnPending();
        }
        return null;
    }

    function renderFulfilled(value: T): RenderResult {
        if (props.children) {
            return props.children(value, createPromise);
        }
        if (props.renderOnFulfilled) {
            return props.renderOnFulfilled(value, createPromise);
        }
        if (defaults.renderOnFulfilled) {
            return defaults.renderOnFulfilled(value, createPromise);
        }
        return null;
    }

    function renderRejected(reason: any): RenderResult {
        if (props.renderOnRejected) {
            return props.renderOnRejected(reason, createPromise);
        }
        if (defaults.renderOnRejected) {
            return defaults.renderOnRejected(reason, createPromise);
        }
        return null;
    }

    switch (state.status) {
        case PromiseStatus.NotCreated: {
            if (createOnNotCreated) {
                createOnNotCreated(createPromise);
            }
            return renderNotCreated();
        }
        case PromiseStatus.Pending: {
            return renderPending();
        };
        case PromiseStatus.Fulfilled: {
            return renderFulfilled(state.value);
        }
        case PromiseStatus.Rejected: {
            return renderRejected(state.reason);
        }
    }

}

export type PromiseSwitchDefaults = {
    renderOnNotCreated?: (createPromise: CreatePromiseFunction<any, any>) => RenderResult;
    renderOnPending?: () => RenderResult;
    renderOnRejected?: (reason: any, createPromise: CreatePromiseFunction<any, any>) => RenderResult;
    renderOnFulfilled?: (value: any, createPromise: CreatePromiseFunction<any, any>) => RenderResult;
}

const PromiseSwitchContext = React.createContext<PromiseSwitchDefaults>({});

type ProviderProps = {
    defaults: PromiseSwitchDefaults;
    children: JSX.Element;
}

export function PromiseSwitchDefaultsProvider(props: ProviderProps) {
    const { defaults, children } = props;
    return (
        <PromiseSwitchContext.Provider value={defaults}>
            {children}
        </PromiseSwitchContext.Provider>
    )
}

