import { useState } from "react";

export type PromiseState<T> = PromiseNotCreatedState | PromisePendingState | PromiseFulfilledState<T> | PromiseRejectedState;

export type PromiseFulfilledState<T> = {
    status: PromiseStatus.Fulfilled;
    value: T;
}

export type PromiseRejectedState = {
    status: PromiseStatus.Rejected;
    reason: any;
}

export type PromiseNotCreatedState = {
    status: PromiseStatus.NotCreated;
}

export type PromisePendingState = {
    status: PromiseStatus.Pending
}

export type CreatePromiseFunction<T, A extends any[]> = (...args: A) => Promise<T>;

export enum PromiseStatus {
    NotCreated = 'NOT_CREATED',
    Pending = 'PENDING',
    Fulfilled = 'FULFILLED',
    Rejected = 'REJECTED',
}

export function usePromise<T, A extends any[]>(createPromise: CreatePromiseFunction<T, A>): [PromiseState<T>, CreatePromiseFunction<T, A>] {

    function createPromiseWrapper(...args: A) {
        return new Promise<T>((resolve, reject) => {
            setState({
                status: PromiseStatus.Pending
            });
            createPromise(...args).then(
                (value) => {
                    setState({
                        status: PromiseStatus.Fulfilled,
                        value,
                    });
                    resolve(value);
                }
            ).catch(
                (reason) => {
                    setState({
                        status: PromiseStatus.Rejected,
                        reason,
                    });
                    reject(reason);
                }
            )
        })
    }

    const defaultState: PromiseNotCreatedState = {
        status: PromiseStatus.NotCreated,
    }

    const [state, setState] = useState<PromiseState<T>>(defaultState);

    return [state, createPromiseWrapper];

}


